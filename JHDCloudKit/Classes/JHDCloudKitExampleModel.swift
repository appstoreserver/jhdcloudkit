//
//  JHDCloudKitExampleModel.swift
//
// Copyright (c) 2020 Jan-Hendrik Damerau (https://bitbucket.org/appstoreserver/jhdcloudkit/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if os(iOS)
import UIKit
#elseif os(OSX)
import Cocoa
#else
/// Not yet supported
#endif
import CloudKit

class JHDCloudKitExampleModel: JHDCloudKitProtocol {
 
    /// Please provide the database table name here
    static var recordDatabase: String = "ExampleDatabase"
    var record: CKRecord
    
    /// Set all your properties as optionals here
    var exampleString: String?
    var exampleInt: Int?
    
    /// Set your cloud keys here
    struct CKRecordKeys {
        static let field_1 = "exampleFieldNameForThisValue1"
        static let field_2 = "exampleFieldNameForThisValue2"
    }
    
    /// Map your CKRecord properties here
    required init?(record: CKRecord) {

        /// If something mandatory, you can go by guard
        guard let exampleInt = record[CKRecordKeys.field_1] as? Int
        else {
            return nil
        }
        
        /// Set record
        self.record = record
        
        /// Set the mandatory properties
        self.exampleInt = exampleInt
        
        /// Set the optional properties and cast for your need
        self.exampleString = record[CKRecordKeys.field_2] as? String
    }
    
    /// Set back your values to your record
    /// This method is called by before sving data to cloud automaticalle
    /// Use your record property to map all values
    func transfer() -> CKRecord {
        
        record[CKRecordKeys.field_1] = exampleString
        record[CKRecordKeys.field_2] = exampleInt
        
        return record
    }
    
    /// These inits for creating empty models
    convenience init?() {
        
        let uniqUUID = CKRecord.ID(recordName: "\(UUID())")
        let record = CKRecord(recordType: Self.recordDatabase, recordID: uniqUUID)
        
        self.init(record: record)
    }

    convenience init?(uniqueId: Int) {
        
        let uniqUUID = CKRecord.ID(recordName: "\(uniqueId)")
        let record = CKRecord(recordType: Self.recordDatabase, recordID: uniqUUID)
        
        self.init(record: record)
    }
}
