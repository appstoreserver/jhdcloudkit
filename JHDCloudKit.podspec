#
# Be sure to run `pod lib lint JHDCloudKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name           			= 'JHDCloudKit'
  spec.version        			= "0.0.2"
  spec.summary        			= "An open source framework for CloudKit written in Swift and inspired by Apple's CloudKit framework"
  spec.ios.deployment_target 	= '14.0'
  spec.osx.deployment_target  	= '10.14'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  spec.description      = <<-DESC
  An open source framework for CloudKit written in Swift and inspired by Apple's CloudKit framework
                       DESC

  spec.homepage            = 'https://bitbucket.org/appstoreserver/jhdcloudkit'
  spec.license             = { :type => 'No License', :file => 'LICENSE' }

  spec.author              = { 'Jan-Hendrik' => 'jan.damerau@gmail.com' }

  spec.source              = { :git => 'https://appstoreserver@bitbucket.org/appstoreserver/jhdcloudkit.git', :tag => spec.version.to_s }


  spec.source_files  = 'JHDCloudKit/Classes/**/*.{h,m,swift,xib}'
  spec.resources     = ['JHDCloudKit/Assets/**/*']
  spec.frameworks     = 'CloudKit'
  spec.ios.framework  = 'UIKit'
  spec.osx.framework  = 'Cocoa'
end
