//
//  JHDCloudKit.swift
//
// Copyright (c) 2020 Jan-Hendrik Damerau (https://bitbucket.org/appstoreserver/jhdcloudkit/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation
import CloudKit

#if os(iOS)
import UIKit
#elseif os(OSX)
import Cocoa
#else
/// Not yet supported
#endif


@objc public class JHDCloudKit : NSObject {
    // MARK: -
    // MARK: PUBLIC Interface
    // MARK: -
    // MARK: Properties
    
    /// Set your database information.
    /// The default JHDCloudKitDatabaseType is private
    ///
    /// - Parameter identifier: String
    /// - Parameter databaseType: JHDCloudKitDatabaseType
    ///
    public class func database(identifier: String, databaseType: JHDCloudKitDatabaseType = .private) {
        JHDCloudKitManager.defaultManager.CLOUDKIT_CONTAINER = identifier
        JHDCloudKitManager.defaultManager.CLOUDKIT_DATABASETYPE = databaseType
    }
    
    /// Fetch all records from database. To do so, setup a model based on JHDCloudKitProtocol.
    ///
    /// - Parameter resultsLimit: Int
    /// - Parameter timeout: TimeInterval
    ///
    public class func fetchArray<T>(resultsLimit: Int = 100, timeout: TimeInterval = 60, completion: @escaping (Result<[T], Error>) -> Void) where T: JHDCloudKitProtocol {
        JHDCloudKitManager.defaultManager.fetchArray(resultsLimit: resultsLimit, timeout: timeout) { (result: Result<[T], Error>) in
            completion(result)
        }
    }
    
    /// Save a record to database. To do so, use a model based on JHDCloudKitProtocol.
    ///
    /// - Parameter data: JHDCloudKitProtocol
    ///
    public class func save<T>(data: T, completion: @escaping (Result<T, Error>) -> Void) where T: JHDCloudKitProtocol {
        JHDCloudKitManager.defaultManager.save(data: data) { (result: Result<T, Error>) in
            completion(result)
        }
    }
    
    /// Delete a record from database. To do so, use a model based on JHDCloudKitProtocol.
    ///
    /// - Parameter data: JHDCloudKitProtocol
    ///
    public class func delete<T>(data: T, completion: @escaping (Result<Bool, Error>) -> Void) where T: JHDCloudKitProtocol {
        JHDCloudKitManager.defaultManager.delete(data: data) { (result: Result<Bool, Error>) in
            completion(result)
        }
    }
    
}
// MARK: -
// MARK: PRIVATE Interface

/// Define the bundle to get acces to its assets
public final class JHDCloudKitBundle: NSObject {
    public static let bundle = Bundle(for: JHDCloudKitBundle.self)
}

/// Defined enum for the database types
public enum JHDCloudKitDatabaseType {
    case `private`
    case shared
}

/// Protocol shared by all models
public protocol JHDCloudKitProtocol {
    static var recordDatabase: String { get }
    var record: CKRecord { get }
   // associatedtype Keys: CKRecordKeys
    // var storage: [Keys: String] { get set }
    func transfer() -> CKRecord
    
    init?(record: CKRecord)
}

public protocol CKRecordKeys: Hashable, RawRepresentable {
    var rawValue: String { get }
}

@objc private class JHDCloudKitBase : NSObject { }

@objc private class JHDCloudKitManager : JHDCloudKitBase {
    
    /// This is the cloud kit container. get it drom your developer portal,
    /// eg. "iCloud.com.example.project"
    fileprivate var CLOUDKIT_CONTAINER: String?
    /// Database Type
    fileprivate var CLOUDKIT_DATABASETYPE: JHDCloudKitDatabaseType = .private
    
    /// This method returns teh database or exit the app if there is no database information
    fileprivate func database() -> CKDatabase? {
        
        guard let identifier = JHDCloudKitManager.defaultManager.CLOUDKIT_CONTAINER else {
            let reason: String = "Please provide the cloud database container indentifier by using 'JHDCloudKit.database(identifier: \"iCloud.com.example.project\")' in your AppDelegate"
            NSException(name:NSExceptionName(rawValue: "Cloud container missing"), reason:reason, userInfo:nil).raise()
            return nil
        }
        
        switch JHDCloudKitManager.defaultManager.CLOUDKIT_DATABASETYPE {
            case .shared:
                return CKContainer(identifier: identifier).sharedCloudDatabase
            default:
                return CKContainer(identifier: identifier).privateCloudDatabase
        }
    }
    
    /// This method returns a list of all records.
    ///
    /// - Parameter resultsLimit: Int
    /// - Parameter timeout: TimeInterval
    ///
    func fetchArray<T>(resultsLimit: Int = 100, timeout: TimeInterval = 60,
                       completion: @escaping (Result<[T], Error>) -> Void) where T: JHDCloudKitProtocol {
        
        guard let database = JHDCloudKitManager.defaultManager.database() else { return }
        
        DispatchQueue.global().async {
            let query = CKQuery(
                recordType: T.recordDatabase, predicate: NSPredicate(value: true)
            )
            let semaphore = DispatchSemaphore(value: 0)
            var records = [T]()
            var error: Error?
            
            var operation = CKQueryOperation(query: query)
            operation.resultsLimit = resultsLimit
            operation.recordFetchedBlock = {
                if let t = T(record: $0) {
                    records.append(t)
                }
                
            }
            operation.queryCompletionBlock = { (cursor, err) in
                guard err == nil, let cursor = cursor else {
                    error = err
                    semaphore.signal()
                    return
                }
                let newOperation = CKQueryOperation(cursor: cursor)
                newOperation.resultsLimit = operation.resultsLimit
                newOperation.recordFetchedBlock = operation.recordFetchedBlock
                newOperation.queryCompletionBlock = operation.queryCompletionBlock
                operation = newOperation
                database.add(newOperation)
            }
            database.add(operation)
            
            _ = semaphore.wait(timeout: .now() + 60)
            
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(records))
                }
            }
        }
    }
    
    /// This method saves a record from cloud
    ///
    /// - Parameter data: JHDCloudKitProtocol
    ///
    func save<T>(data: T, completion: @escaping (Result<T, Error>) -> Void) where T: JHDCloudKitProtocol {
        
        guard let database = JHDCloudKitManager.defaultManager.database() else { return }

        let record = data.transfer()
      
        database.save(record) { (record, error) -> Void in
            
            DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    completion(.failure(error))
                } else {
                    completion(.success(data))
                }
            }
        }
    }
    
    
    /// This method deletes a record from cloud
    ///
    /// - Parameter data: JHDCloudKitProtocol
    ///
    func delete<T>(data: T, completion: @escaping (Result<Bool, Error>) -> Void) where T: JHDCloudKitProtocol {
        
        guard let database = JHDCloudKitManager.defaultManager.database() else { return }
        
        let recordID = data.record.recordID
        database.delete(withRecordID: recordID) { (recordID, error) in
            
            DispatchQueue.main.async {
                if let error = error {
                    print(error.localizedDescription)
                    completion(.failure(error))
                } else {
                    completion(.success(true))
                }
            }
        }
    }
    
    // MARK: -
    // MARK: Singleton
    public class var defaultManager: JHDCloudKitManager {
        struct Singleton {
            static let instance: JHDCloudKitManager = JHDCloudKitManager()
        }
        return Singleton.instance
    }
    
    override init() { super.init() }
}
