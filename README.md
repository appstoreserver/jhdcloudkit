# JHDCloudKit

An open source framework for CloudKit written in Swift and inspired by Apple's CloudKit framework

## Requirements

- Xcode 11.0+
- iOS 14.0+, Mac OS X 10.14+

## Installation

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```
> CocoaPods 0.39.0+ is required to build JHDCloudKit.

To integrate JHDPagesKit into your Xcode project using CocoaPods, specify it in your `Podfile`:

```bash
pod 'JHDCloudKit', :git => 'https://bitbucket.org/appstoreserver/jhdcloudkit.git'
```

Then, run the following command:

```bash
$ pod install
```

Create for all your database tables a model based on JHDCloudKitProtocol. Here is an example:

```swift

import UIKit
import CloudKit

class JHDCloudKitExampleModel: JHDCloudKitProtocol {
 
    /// Please provide the database table name here
    static var recordDatabase: String = "ExampleDatabase"
    var record: CKRecord
    
    /// Set all your properties as optionals here
    var exampleString: String?
    var exampleInt: Int?
    
    /// Set your cloud keys here
    struct CKRecordKeys {
        static let field_1 = "exampleFieldNameForThisValue1"
        static let field_2 = "exampleFieldNameForThisValue2"
    }
    
    /// Map your CKRecord properties here
    required init?(record: CKRecord) {

        /// If something mandatory, you can go by guard
        guard let exampleInt = record[CKRecordKeys.field_1] as? Int
        else {
            return nil
        }
        
        /// Set record
        self.record = record
        
        /// Set the mandatory properties
        self.exampleInt = exampleInt
        
        /// Set the optional properties and cast for your need
        self.exampleString = record[CKRecordKeys.field_2] as? String
    }
    
    /// set back your values to your record
    func transfer() -> CKRecord {
        
        record[CKRecordKeys.field_1] = exampleString
        record[CKRecordKeys.field_2] = exampleInt
        
        return record
    }
}
```

You can add some convience init to your model as well:

```swift

convenience init?() {
    
    let uniqUUID = CKRecord.ID(recordName: "\(UUID())")
    let record = CKRecord(recordType: Self.recordDatabase, recordID: uniqUUID)
    
    self.init(record: record)
}

convenience init?(uniqueId: Int) {
    
    let uniqUUID = CKRecord.ID(recordName: "\(uniqueId)")
    let record = CKRecord(recordType: Self.recordDatabase, recordID: uniqUUID)
    
    self.init(record: record)
}
```

Set in your AppDelegate the cloud information once:

```swift

func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    // CloudKit Setup
    JHDCloudKit.database(identifier: "iCloud.com.example.project")
    
    return true
}
```

To fetch all data from a databse, you can use **JHDCloudKit.fetchArray()** by providing the model, e.g with JHDCloudKitExampleModel
```swift

JHDCloudKit.fetchArray() { (result: Result<[JHDCloudKitExampleModel], Error>) in
    switch result {
        case .success(let items):
            // Handle fetched items
            print(items)
        case .failure(let error):
            // Handle Error
            print(error)
    }
}
```

To save a record, you can use **JHDCloudKit.save(data: JHDCloudKitProtocol)** by providing a record based on JHDCloudKitProtocol which as to be saved 
```swift 
let item: JHDCloudKitExampleModel = JHDCloudKitExampleModel() // you need this record first

JHDCloudKit.save(data: item){ (result: Result<NotificationCloudModel, Error>) in
    switch result {
        case .success(let data):
            break;
        case .failure(let error):
            // Handle Error
            print(error)
    }
}
```

To delete a record, you can use **JHDCloudKit.delete(data: JHDCloudKitProtocol)** by providing a record based on JHDCloudKitProtocol which as to be deleted 
```swift 
let item: JHDCloudKitExampleModel = JHDCloudKitExampleModel() // you need this record first

JHDCloudKit.delete(data: item)  { (result: Result<Bool, Error>) in
    switch result {
    case .success(let bool):
        /// do your stuff
        break;
    case .failure(let error):
        print(error)
        break;
    }
}
```

## Author

Jan-H. Damerau <app@jandamerau.com>

## License

JHDCloudKit is released under an MIT license. See [LICENSE](https://bitbucket.org/appstoreserver/jhdcloudkit/src/master/LICENSE) for more information.
